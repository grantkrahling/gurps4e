/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class gurpsActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["gurps4e", "sheet", "actor"],
      template: "systems/gurps4e/templates/actor/actor-sheet.html",
      width: 540,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "stats" }]
    });
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];
    for (let attr of Object.values(data.data.secondaryAttributes)) {
      attr.isCheckbox = attr.dtype === "Boolean";
    }
    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Rollable checks.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Plus - Minus check
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));
  
    // Relative updates for numeric fields (from DnD5e)
    //inputs.find('input[data-dtype="Number"]').change(this._onChangeInputDelta.bind(this));

    // track and handle changes to HP and FP
    html.find('.sec-attr').change(this._onSecondaryAttributeChange.bind(this));
  }

  /* -------------------------------------------- */

  
  /**
   * Handle changes to secondary attribute number fields.
   * @param {Event} event   The originating change event
   * @private
   */
  _onSecondaryAttributeChange(event) {
    event.preventDefault();
    let value = event.target.value;
    let name = event.target.name;
    this.actor.setConditions(value, name);
  }

  /**
   * Handle the behaviour of the plus and minus 'buttons' related to a label.
   * @param {Event} event   The originating click event
   * @private
   */
  _onPlusMinus(event) {
    event.preventDefault();
    let field = event.currentTarget.firstElementChild;
    let fieldName = field.name;
    let change = parseInt(field.value);
    var value;
    var fieldValue;
    
    if (fieldName == "gmod") {
      fieldValue = "data.gmod.value";
      value = change + this.actor.data.data.gmod.value;
    } else if (fieldName == "dmod") {
      fieldValue = "data.dmod.value";
      value = change + this.actor.data.data.dmod.value;
    } else {
      fieldValue = "data.attacks." + fieldName + ".seed";
      let damages = this.actor.data.data.attacks;
      
      for (let [id, damage] of Object.entries(damages)) {
        if (fieldName == id) {
          value = ((value = damage.seed + change) == 0) ? 1 : value;
          break;
        }
      }
    }
    this.actor.update({[fieldValue]: value});
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  /**
  * Handle clickable rolls.
  * @param {Event} event   The originating click event
  * @private
  */
  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if (dataset.roll) {
      let r1 = new Roll(dataset.roll, this.actor.data.data);
      let r = new Roll("3d6", this.actor.data.data);
      let isDmgRoll = dataset.label.endsWith("damage.");
      var label;
      var tgt;
      var mos;
      var result;
      r1.roll();
      tgt = r1.total;
      r.roll();
      mos = tgt - r.total;

      if (isDmgRoll) {
        label = "Rolled: <b>" + r1.total + "</b>" + dataset.label;
        r1.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: label
        });
      } else {
        if (r.total === 3 || r.total === 4 || ((r.total === 5 || r.total === 6) && mos >= 10)) {
          result = " <b style=\"color:#00bb00\">Critical Success</b>";
        }
        // A roll of 18 is always a critical failure.
        else if (r.total === 18) {
          result = " <b style=\"color:#dd0000\">Critical Failure</b>";
        }
        // A roll of 17 is a critical failure if your effective skill is 15 or less; otherwise, it is an ordinary failure.
        else if (r.total === 17) {
          result = " <b style=\"color:#dd0000\">Failure</b>";
          if (mos <= -2) {
            result = " <b style=\"color:#dd0000\">Critical Failure</b>";
          }
        }
        // Any roll of 10 greater than your effective skill is a critical failure
        else if (mos <= -10) {
          result = " <b style=\"color:#dd0000\">Critical Failure</b>";
        }
        else if (mos < 0) {
          result = " <b style=\"color:#dd0000\">Failure</b> by: <b>" + (-1 * mos) + "</b>";
        }
        else if (mos >= 0) {
          result = " <b style=\"color:#00bb00\">Success</b> by: <b>" + mos + "</b>";
        }
      
        if (dataset.label) {
          label = "Rolled: <b>" + r.total + "</b>" + dataset.label + result;
        } else {
          label = "Rolled: <b>" + r.total + "</b>" + " vs <b>" + tgt + "</b>" + " with " + result;
        }
      
        r.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: label
        });
      }
    }
    this.actor.update({["data.gmod.value"]: 0});
  }

}
